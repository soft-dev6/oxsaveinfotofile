/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgui;

import java.util.Scanner;

/**
 *
 * @author hanam
 */
public class Game {
     private Player o; 
     private Player x;
     private int row;
     private int col;
     private Board board;
     Scanner kb = new Scanner(System.in);
     public Game(){
         this.o = new Player('O');
         this.x = new Player('X');
     }
     
     public void newBoard(){
         this.board = new Board(o,x);
     }
     
     public void showWelcome(){
         System.out.println("Welcome to OX Game");
     }
     
     public  void showTable(){
         char [][] board = this.board.getTable();
         for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
     }
     
     public void showTurn(){
         Player player = board.getCurrentPlayer();
         System.out.println("Turn "+player.getSymbol());
         System.out.println("Welcome to OX Game");
     } 
     public void inputRowCol(){
         while(true){
         System.out.println("Please intput row, col: ");
         row = kb.nextInt();
         col = kb.nextInt();
         if(board.setRowCol(row, col));
          return;
         }
     }
     
      public boolean isFinish(){
        if(board.isDraw()||board.isWin()){
            return true;
        }
        return false;
    }
      
      public void  showResult(){
          if(board.isDraw()){
              System.out.println(">>>>DRAW<<<<");
          }else if (board.isWin()){
              System.out.println(board.getCurrentPlayer().getSymbol()+" Win");
          }
      }
     
    public void showStat() {
        System.out.println(o);
        System.out.println(x);
    }
}
